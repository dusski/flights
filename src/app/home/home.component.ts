import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  today = new Date()

  searchFlight = {
    'origin': '',
    'destination': '',
    'start_date': {
      'year': this.today.getFullYear(),
      'month': this.today.getMonth() + 1,
      'day': this.today.getDate() + 1
    },
    'end_date': {},
    'adultCount': 1,
    'childCount': 0,
    'infantInSeatCount': 0,
  }

  searchRequest = {}

  ngOnInit() {

  }

  doSearch() {
    // proveri da li ima sva polja
    if (this.searchFlight.adultCount <= 0
      || this.searchFlight.origin.length <= 0
      || this.searchFlight.destination.length <= 0
    ) {
      alert('Nisi upisao u sva obavezna polja!')
      return
    }

    // Procesiraj pretragu
    this.searchRequest = {
      "request": {
        "slice": [
          {
            "origin": "BEG",
            "date": "2017-03-15",
            "destination": "LON"
          },
          {
            "destination": "LON",
            "date": "2017-03-30",
            "origin": "BEG"
          }
        ],
          "passengers": {
          "adultCount": 1,
          "childCount": 1,
          "infantInSeatCount": 1
        },
        "solutions": 20
      }
    }





  }

}
